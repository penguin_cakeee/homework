from math import sqrt
def mean(a, b):
    a = float(a)
    b = float(b)

    ar = (a + b) / 2
    return ar

def geometric_mean(a,b):
    a = float(a)
    b = float(b)

    if a >= 0 and b >= 0:
        ge = sqrt(a * b)
    else:
        ge = "a or b < 0"
    return ge
