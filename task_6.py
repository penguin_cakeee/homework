from random import randrange


def create_matritsa(a, b, n, m):
    matrix = [[randrange(a, b) for i in range(0, n)] for e in range(0, m)]
    return matrix


#matrits = (create_matritsa(-5, 9, 3, 4))


matrits = [[4, 3, 6], [1, 8, 2], [9, 5, 3]]

def max_element(matr):
    max_el = matr[0][0]
    for line in matr:
        for el in line:
            if el > max_el:
                max_el = el
    return max_el


max_elem = max_element(matrits)


def min_element(matr):
    min_el = matr[0][0]
    for line in matr:
        for el in line:
            if el < min_el:
                min_el = el
    return min_el


min_elem = min_element(matrits)


def sum_elements(matr):
    sum_el = 0
    for line in matr:
        for el in line:
            sum_el += el
    return sum_el


sum_elem = sum_elements(matrits)


def max_sum_line(matr):
    max_sum = 0
    i = 0
    max_index = 0
    for line in matr:
        sum_el_line = 0
        for el in line:
            sum_el_line += el
        if sum_el_line > max_sum:
            max_sum = sum_el_line
            max_index = i
        elif i == 0:
            max_sum = sum_el_line
        i += 1
    return max_index


max_line_index = max_sum_line(matrits)


def max_sum_column(matr):
    max_sum = 0
    i = 0
    max_index = 0
    for el in matr[0]:
        sum_el_column = 0
        for line in matr:
            sum_el_column += line[i]
        if sum_el_column > max_sum:
            max_sum = sum_el_column
            max_index = i
        elif i == 0:
            max_sum = sum_el_column
        i += 1
    return max_index


max_column_index = max_sum_column(matrits)


def min_sum_line(matr):
    min_sum = 0
    i = 0
    min_index = 0
    for line in matr:
        sum_el_line = 0
        for el in line:
            sum_el_line += el
        if i == 0:
            min_sum = sum_el_line
        elif sum_el_line < min_sum:
            min_sum = sum_el_line
            min_index = i
        i += 1
    return min_index


min_line_index = min_sum_line(matrits)


def min_sum_column(matr):
    min_sum = 0
    i = 0
    min_index = 0
    for el in matr[0]:
        sum_el_column = 0
        for line in matr:
            sum_el_column += line[i]
        if i == 0:
            min_sum = sum_el_column
        elif sum_el_column < min_sum:
            min_sum = sum_el_column
            min_index = i
        i += 1
    return min_index


min_column_index = min_sum_column(matrits)


def zero_above_diag(matr):
    index_line = 0
    for line in matr:
        index_elem = 0
        for el in line:
            if index_line < index_elem:
                matr[index_line][index_elem] = 0
            index_elem += 1
        index_line += 1
    return matr


zero_above_diagonal = zero_above_diag(matrits)


def zero_only_diag(matr):
    index_line = 0
    for line in matr:
        index_elem = 0
        for el in line:
            if index_line > index_elem:
                matr[index_line][index_elem] = 0
            index_elem += 1
        index_line += 1
    return matr


zero_only_diagonal = zero_only_diag(matrits)


def create_new_matritsa(a, b, n, m):
    matrix_a = [[randrange(a, b) for i in range(0, n)] for e in range(0, m)]
    matrix_b = [[randrange(a, b) for i in range(0, n)] for e in range(0, m)]
    return matrix_a, matrix_b


matrix_a, matrix_b = create_new_matritsa(0, 9, 3, 3)


def sum_matr(matr1, matr2):
    new_sum_matrix = []
    index_line = 0
    for line in matrix_a:
        index_el = 0
        new_line = []
        for el in line:
            el = el + matrix_b[index_line][index_el]
            new_line.append(el)
            index_el += 1
        new_sum_matrix.append(new_line)
        index_line += 1
    return new_sum_matrix


sum_matr_a_b = sum_matr(matrix_a, matrix_b)


def subtract_matr(matr1, matr2):
    new_sum_matrix = []
    index_line = 0
    for line in matrix_a:
        index_el = 0
        new_line = []
        for el in line:
            el = el - matrix_b[index_line][index_el]
            new_line.append(el)
            index_el += 1
        new_sum_matrix.append(new_line)
        index_line += 1
    return new_sum_matrix


sub_matrix_a_b = subtract_matr(matrix_a, matrix_b)


def matrix_a_with_g(matr, g):
    new_matrix = []
    for line in matr:
        new_line = []
        for el in line:
            el = el * g
            new_line.append(el)
        new_matrix.append(new_line)
    return new_matrix


new_matrix_g = matrix_a_with_g(matrix_a, int(input('enter g >>> ')))
print(matrits, max_elem, min_elem, sum_elem, max_line_index, max_column_index, min_line_index, min_column_index)
print(zero_above_diagonal, zero_only_diagonal)
print(matrix_a)
print(matrix_b)
print(sum_matr_a_b)
print(sub_matrix_a_b)
print(new_matrix_g)
