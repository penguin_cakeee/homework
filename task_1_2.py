from math import fabs


def counter(x, y):
    x = fabs(x)
    y = fabs(y)

    rez = (x - y)/(1 + (x*y))
    return rez