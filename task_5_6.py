def mono(mas):
    n = 0
    last = 0
    b = False
    for i in mas:
        if i > last:
            if not b:
                n +=1
            b = True
        else:
            b = False
        last = i

    return n


