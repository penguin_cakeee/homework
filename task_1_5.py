from math import sqrt
def hypotenuse(a, b):
    a = float(a)
    b = float(b)

    square = (a * b) / 2
    g = sqrt((a**2) + (b ** 2))
    return square, g
