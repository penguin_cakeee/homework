def fact2(n):
    sum = 1
    for i in range (1, n + 1):
        if n % 2 == 0:
            if i % 2 == 0:
                sum = sum * i
        else:
            if i % 2 != 0:
                sum = sum * i
    return sum


for i in range(5):
    n = int(input())
    print(fact2(n))
