def duim_santimetr(n):
    return n * 2.54


def santim_duim(n):
    return n / 2.54


def milli_km(n):
    return n / 1.609


def km_milli(n):
    return n * 1.609


def phunt_kg(n):
    return n * 0.453592


def kg_phunt(n):
    return n / 0.453592


def oz_gr(n):
    return n * 28.3495


def gr_oz(n):
    return n / 28.3495


def gallon_litrs(n):
    return n * 3.7854


def litrs_gallon(n):
    return n / 3.7854


def pint_litrs(n):
    return n * 0.5683


def litrs_pint(n):
    return n / 0.5683


while True:
    i = int(input('i = 0-12\n'))
    if i == 1:
        print(duim_santimetr(int(input('\n enter number\n'))))
    elif i == 2:
        print(santim_duim(int(input('\n enter number\n'))))
    elif i == 3:
        print(milli_km(int(input('\n enter number\n'))))
    elif i == 4:
        print(km_milli(int(input('\n enter number\n'))))
    elif i == 5:
        print(phunt_kg(int(input('\n enter number\n'))))
    elif i == 6:
        print(kg_phunt(int(input('\n enter number\n'))))
    elif i == 7:
        print(oz_gr(int(input('\n enter number\n'))))
    elif i == 8:
        print(gr_oz(int(input('\n enter number\n'))))
    elif i == 9:
        print(gallon_litrs(int(input('\n enter number\n'))))
    elif i == 10:
        print(litrs_gallon(int(input('\n enter number\n'))))
    elif i == 11:
        print(pint_litrs(int(input('\n enter number\n'))))
    elif i == 12:
        print(litrs_pint(int(input('\n enter number\n'))))
    elif i == 0:
        print('end')
        break
    else:
        print('error')
